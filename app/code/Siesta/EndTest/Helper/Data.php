<?php


namespace Siesta\EndTest\Helper;


use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Siesta\EndTest\Model\Changes;
use Siesta\EndTest\Model\ChangesRepository;
use Magento\Backend\Model\Auth\Session;

class Data extends AbstractHelper
{
    /**
     * @var Changes
     */
    private $changes;

    /**
     * @var ChangesRepository
     */
    private $changesRepository;

    /**
     * @var Session
     */
    private $authSession;

    /**
     * Data constructor.
     * @param Changes $changes
     * @param ChangesRepository $changesRepository
     * @param Session $authSession
     * @param Context $context
     */
    public function __construct(
        Changes $changes,
        ChangesRepository $changesRepository,
        Session $authSession,
        Context $context

    ) {
        $this->changes = $changes;
        $this->changesRepository = $changesRepository;
        $this->authSession = $authSession;

        parent::__construct($context);
    }

    /**
     * @param string $productSku
     * @param int $oldStock
     * @param int $newStock
     * @throws \Exception
     */
    public function stockChanges($productSku, $oldStock, $newStock)
    {
        $changesModel = $this->changes;
        $changesModel->setProductSku($productSku);
        $changesModel->setNewStock($newStock);
        $changesModel->setOldStock($oldStock);
        $changesModel->setCreateTime(new \DateTime('now'));
        $user = $this->getCurrentUser();

        if ($user && $user->getRole()->getData()['role_name'] == "Administrators") {
            $changesModel->setRunBy("Product quantity has been changed by Admin ({$user->getId()})");
        }

        try {
            $this->changesRepository->save($this->changes);
        } catch (\Exception $e) {
            throw $e;
        }
    }

    /**
     * @return \Magento\User\Model\User
     */
    public function getCurrentUser()
    {
        return $this->authSession->getUser();
    }
}