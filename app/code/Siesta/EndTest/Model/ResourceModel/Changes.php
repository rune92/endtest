<?php
/**
 * Created by PhpStorm.
 * User: davidkovacs
 * Date: 2017. 09. 17.
 * Time: 15:25
 */

namespace Siesta\EndTest\Model\ResourceModel;


use Magento\Framework\Model\ResourceModel\Db\AbstractDb;
use Siesta\EndTest\Model\Data\ChangesInterface;

class Changes extends AbstractDb
{
    public function _construct()
    {
        $this->_init(ChangesInterface::TABLE, ChangesInterface::COLUMN_ID);
    }
}