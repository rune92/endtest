<?php
/**
 * Created by PhpStorm.
 * User: davidkovacs
 * Date: 2017. 09. 17.
 * Time: 15:27
 */

namespace Siesta\EndTest\Model\ResourceModel\Changes;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

class Collection extends AbstractCollection
{
    /**
     * Initialize resource collection
     *
     * @return void
     */
    public function _construct()
    {
        $this->_init('Siesta\EndTest\Model\Changes', 'Siesta\EndTest\Model\ResourceModel\Changes');
    }
}