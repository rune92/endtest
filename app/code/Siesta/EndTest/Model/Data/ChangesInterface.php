<?php

/**
 * Created by PhpStorm.
 * User: davidkovacs
 * Date: 2017. 09. 17.
 * Time: 15:15
 */
namespace Siesta\EndTest\Model\Data;

interface ChangesInterface
{
    const
        TABLE = 'siesta_endtest_changes',
        COLUMN_ID = 'id',
        COLUMN_PRODUCT_SKU = 'sku',
        COLUMN_OLD_STOCK_QUANTITY = 'oldStockQuantity',
        COLUMN_NEW_STOCK_QUANTITY = 'newStockQuantity',
        COLUMN_CREATE_TIME = 'createTime',
        COLUMN_RUN_BY = 'runBy'
    ;
}