<?php
/**
 * Created by PhpStorm.
 * User: davidkovacs
 * Date: 2017. 09. 17.
 * Time: 15:50
 */

namespace Siesta\EndTest\Model;


use Magento\Framework\App\Config\BaseFactory;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Store\Model\StoreManagerInterface;
use Siesta\EndTest\Api\ChangesRepositoryInterface;
use Siesta\EndTest\Model\ResourceModel\Changes;
use Siesta\EndTest\Model\Changes as ChangesModel;

class ChangesRepository implements ChangesRepositoryInterface
{
    /**
     * @var StoreManagerInterface
     */
    protected $storeManager;

    /**
     * @var Changes
     */
    protected $resource;

    /**
     * @var BaseFactory
     */
    protected $changesFactory;

    /**
     * ChangesRepository constructor.
     * @param ResourceChanges $resource
     * @param ChangesFactory $changesFactory
     * @param StoreManagerInterface $storeManager
     */
    public function __construct(
        Changes $resource,
        ChangesFactory $changesFactory,
        StoreManagerInterface $storeManager
    )
    {
        $this->resource = $resource;
        $this->storeManager = $storeManager;
        $this->changesFactory = $changesFactory;
    }

    /**
     * @param int $id
     * @return ChangesModel
     * @throws NoSuchEntityException
     */
    public function getById($id)
    {
        /** @var ChangesModel $changes */
        $changes = $this->changesFactory->create();
        $this->resource->load($changes, $id);

        if (!$changes->getId()) {
            throw new NoSuchEntityException(__('Changes with id "%1" does not exist.', $id));
        }

        return $changes;
    }

    /**
     * @param ChangesModel $changes
     * @return ChangesModel
     * @throws \Exception
     */
    public function save(ChangesModel $changes)
    {
        $this->resource->save($changes);

        return $changes;
    }

    /**
     * @param ChangesModel $changes
     * @throws \Exception
     */
    public function delete(ChangesModel $changes)
    {
        $this->resource->delete($changes);
    }

    /**
     * @param $id
     * @throws \Exception
     */
    public function deleteById($id)
    {
        /** @var ChangesModel $changes */
        $changes = $this->changesFactory->create();
        $this->resource->load($changes, $id);

        if (!$changes->getId()) {
            throw new NoSuchEntityException(__('Changes with id "%1" does not exist.', $id));
        }

        $this->resource->delete($changes);
    }

    /**
     * @param ChangesModel $changes
     */
    protected function setAllSubValuesToValue(ChangesModel &$changes)
    {
        $data = $changes->getData();

        foreach ($data as $dataKey => $dataValue) {
            if (isset($dataValue['value'])) {
                $changes->setData($dataKey, $dataValue['value']);
            } else {
                $changes->setData($dataKey, $dataValue);
            }
        }
    }
}