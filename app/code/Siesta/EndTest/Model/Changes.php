<?php
/**
 * Created by PhpStorm.
 * User: davidkovacs
 * Date: 2017. 09. 17.
 * Time: 15:14
 */

namespace Siesta\EndTest\Model;

use Magento\EncryptionKey\Model\ResourceModel\Key\Change;
use Magento\Framework\Model\AbstractModel;
use Siesta\EndTest\Model\Data\ChangesInterface;
use Magento\Framework\Model\Context;
use Magento\Framework\Registry;
use Magento\Framework\Model\ResourceModel\AbstractResource;
use Magento\Framework\Data\Collection\AbstractDb;
use Siesta\EndTest\Model\ResourceModel\Changes as ChangesResourceModel;

/**
 * Class Changes
 * @package Siesta\EndTest\Model
 */
class Changes extends AbstractModel implements ChangesInterface
{
    /**
     * @param Context $context
     * @param Registry $registry
     * @param AbstractResource|null $resource
     * @param AbstractDb|null $resourceCollection
     * @param array $data
     */
    public function __construct(
        Context $context,
        Registry $registry,
        AbstractResource $resource = null,
        AbstractDb $resourceCollection = null,
        array $data = []
    )
    {
        parent::__construct($context, $registry, $resource, $resourceCollection, $data);
    }

    /**
     * @return void
     */
    public function _construct()
    {
        $this->_init(ChangesResourceModel::class);
    }

    /**
     * @return string
     */
    public function getProductSku()
    {
        return $this->getData(ChangesInterface::COLUMN_PRODUCT_SKU);
    }

    /**
     * @param $sku
     * @return $this
     */
    public function setProductSku($sku)
    {
        $this->setData(ChangesInterface::COLUMN_PRODUCT_SKU, $sku);
        
        return $this;
    }

    /**
     * @return integer
     */
    public function getOldStock()
    {
        return $this->getData(ChangesInterface::COLUMN_OLD_STOCK_QUANTITY);
    }

    /**
     * @param integer $oldStock
     * @return $this
     */
    public function setOldStock($oldStock)
    {
        $this->setData(ChangesInterface::COLUMN_OLD_STOCK_QUANTITY, $oldStock);
        
        return $this;
    }

    /**
     * @return integer
     */
    public function getNewStock()
    {
        return $this->getData(ChangesInterface::COLUMN_NEW_STOCK_QUANTITY);
    }

    /**
     * @param $newStock
     * @return $this
     */
    public function setNewStock($newStock)
    {
        $this->setData(ChangesInterface::COLUMN_NEW_STOCK_QUANTITY, $newStock);
        
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getCreateTime()
    {
        return $this->getData(ChangesInterface::COLUMN_CREATE_TIME);
    }

    /**
     * @param $createTime
     * @return $this
     */
    public function setCreateTime($createTime)
    {
        $this->setData(ChangesInterface::COLUMN_CREATE_TIME, $createTime);

        return $this;
    }

    /**
     * @return string
     */
    public function getRunBy()
    {
        return $this->getData(ChangesInterface::COLUMN_RUN_BY);
    }

    /**
     * @param string $runBy
     * @return $this
     */
    public function setRunBy($runBy)
    {
        $this->setData(ChangesInterface::COLUMN_RUN_BY, $runBy);

        return $this;
    }
}