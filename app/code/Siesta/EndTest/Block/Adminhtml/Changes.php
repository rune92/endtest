<?php
/**
 * Created by PhpStorm.
 * User: davidkovacs
 * Date: 2017. 09. 17.
 * Time: 17:11
 */

namespace Siesta\EndTest\Block\Adminhtml;

use Magento\Backend\Block\Widget\Grid\Container;

class Changes extends Container
{
    /**
     * Constructor
     * @return void
     */
    protected function _construct()
    {
        parent::_construct();

        $this->_controller = 'adminhtml_changes';
        $this->_blockGroup = 'Siesta_EndTest';
        $this->_headerText = __('Changes');
    }
}