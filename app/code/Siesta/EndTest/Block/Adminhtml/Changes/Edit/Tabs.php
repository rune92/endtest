<?php
/**
 * Created by PhpStorm.
 * User: davidkovacs
 * Date: 2017. 09. 17.
 * Time: 17:01
 */

namespace Siesta\EndTest\Block\Adminhtml\Changes\Edit;

class Tabs extends \Magento\Backend\Block\Widget\Tabs
{
    protected function _construct()
    {
        parent::_construct();
        $this->setId('endtest_changes_tabs');
        $this->setDestElementId('edit_form');
        $this->setTitle(__('Changes Information'));
    }
}