<?php
/**
 * Created by PhpStorm.
 * User: davidkovacs
 * Date: 2017. 09. 17.
 * Time: 17:00
 */

namespace Siesta\EndTest\Block\Adminhtml\Changes;

use Magento\Backend\Block\Widget\Grid\Extended;
use Siesta\EndTest\Model\Data\ChangesInterface;
use Siesta\EndTest\Model\ResourceModel\Changes\Collection;

class Grid extends Extended
{
    /**
     * @var \Magento\Framework\Module\Manager
     */
    protected $moduleManager;

    /**
     * @var \Magento\Eav\Model\ResourceModel\Entity\Attribute\Set\CollectionFactory
     */
    protected $_setsFactory;

    /**
     * @var \Magento\Catalog\Model\ProductFactory
     */
    protected $_productFactory;

    /**
     * @var \Magento\Catalog\Model\Product\Type
     */
    protected $_type;

    /**
     * @var \Magento\Catalog\Model\Product\Attribute\Source\Status
     */
    protected $_status;

    protected $_collectionFactory;

    /**
     * @var \Magento\Catalog\Model\Product\Visibility
     */
    protected $_visibility;

    /**
     * @var \Magento\Store\Model\WebsiteFactory
     */
    protected $_websiteFactory;

    /**
     * Grid constructor.
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\Backend\Helper\Data $backendHelper
     * @param \Magento\Store\Model\WebsiteFactory $websiteFactory
     * @param Collection $collectionFactory
     * @param \Magento\Framework\Module\Manager $moduleManager
     * @param array $data
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Backend\Helper\Data $backendHelper,
        \Magento\Store\Model\WebsiteFactory $websiteFactory,
        Collection $collectionFactory,
        \Magento\Framework\Module\Manager $moduleManager,
        array $data = []
    )
    {

        $this->_collectionFactory = $collectionFactory;
        $this->_websiteFactory = $websiteFactory;
        $this->moduleManager = $moduleManager;
        parent::__construct($context, $backendHelper, $data);
    }

    /**
     * @return void
     */
    protected function _construct()
    {
        parent::_construct();

        $this->setId('endtest_changes_container');
        $this->setDefaultSort('id');
        $this->setDefaultDir('DESC');
        $this->setSaveParametersInSession(true);
        $this->setUseAjax(true);

    }

    /**
     * @return Store
     */
    protected function _getStore()
    {
        $storeId = (int)$this->getRequest()->getParam('store', 0);
        return $this->_storeManager->getStore($storeId);
    }

    /**
     * @return $this
     */
    protected function _prepareCollection()
    {
        try {
            $collection = $this->_collectionFactory->load();
            $this->setCollection($collection);

            parent::_prepareCollection();

            return $this;
        } catch (\Exception $e) {
            echo $e->getMessage();
            die;
        }
    }

    /**
     * @param \Magento\Backend\Block\Widget\Grid\Column $column
     * @return $this
     */
    protected function _addColumnFilterToCollection($column)
    {
        if ($this->getCollection()) {
            if ($column->getId() == 'websites') {
                $this->getCollection()->joinField(
                    'websites',
                    'catalog_product_website',
                    'website_id',
                    'product_id=entity_id',
                    null,
                    'left'
                );
            }
        }
        return parent::_addColumnFilterToCollection($column);
    }

    /**
     * @return $this
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    protected function _prepareColumns()
    {
        $this->addColumn(
            ChangesInterface::COLUMN_ID,
            [
                'header' => __('ID'),
                'type' => 'number',
                'index' => ChangesInterface::COLUMN_ID,
                'header_css_class' => 'col-id',
                'column_css_class' => 'col-id'
            ]
        );

        $this->addColumn(
            ChangesInterface::COLUMN_OLD_STOCK_QUANTITY,
            [
                'header' => __('Old Stock Quantity'),
                'index' => ChangesInterface::COLUMN_OLD_STOCK_QUANTITY,
                'type' => 'number',
            ]
        );

        $this->addColumn(
            ChangesInterface::COLUMN_NEW_STOCK_QUANTITY,
            [
                'header' => __('New Stock Quantity'),
                'index' => ChangesInterface::COLUMN_NEW_STOCK_QUANTITY,
                'type' => 'number',
            ]
        );

        $this->addColumn(
            ChangesInterface::COLUMN_PRODUCT_SKU,
            [
                'header' => __('SKU'),
                'index' => ChangesInterface::COLUMN_PRODUCT_SKU,
                'type' => 'text',
            ]
        );

        $this->addColumn(
            ChangesInterface::COLUMN_RUN_BY,
            [
                'header' => __('Run by'),
                'index' => ChangesInterface::COLUMN_RUN_BY,
                'type' => 'text',
            ]
        );

        $this->addColumn(
            ChangesInterface::COLUMN_CREATE_TIME,
            [
                'header' => __('Create time'),
                'index' => ChangesInterface::COLUMN_CREATE_TIME,
                'type' => 'datetime',
            ]
        );

        $block = $this->getLayout()->getBlock('grid.bottom.links');

        if ($block) {
            $this->setChild('grid.bottom.links', $block);
        }

        return parent::_prepareColumns();
    }

    /**
     * @return $this
     */
    protected function _prepareMassaction()
    {
        $this->setMassactionIdField('id');
        $this->getMassactionBlock()->setFormFieldName('id');

        return $this;
    }

    /**
     * @return string
     */
    public function getGridUrl()
    {
        return $this->getUrl('changes/*/index', ['_current' => true]);
    }

    /**
     * @param \Magento\Catalog\Model\Product|\Magento\Framework\Object $row
     * @return string
     */
    public function getRowUrl($row)
    {
        return $this->getUrl(
            'changes/*/edit',
            ['store' => $this->getRequest()->getParam('store'), 'id' => $row->getId()]
        );
    }
}