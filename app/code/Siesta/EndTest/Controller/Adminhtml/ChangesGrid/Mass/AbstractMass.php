<?php
/**
 * Created by PhpStorm.
 * User: davidkovacs
 * Date: 2017. 09. 17.
 * Time: 16:40
 */

namespace Siesta\EndTest\Controller\Adminhtml\ChangesGrid\Mass;

use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Ui\Component\MassAction\Filter;
use Siesta\EndTest\Model\ChangesRepository;
use Siesta\EndTest\Model\ResourceModel\Changes\CollectionFactory;

/**
 * Class AbstractMass
 *
 * @package Oander\MultiSlider\Controller\Adminhtml\Image
 */
abstract class AbstractMass extends Action
{
    /**
     * @var Filter
     */
    protected $filter;

    /**
     * @var CollectionFactory
     */
    protected $collectionFactory;

    /**
     * @var ChangesRepository
     */
    protected $changesRepository;

    /**
     * @param Context $context
     * @param Filter $filter
     * @param CollectionFactory $collectionFactory
     * @param ChangesRepository $imageRepository
     */
    public function __construct(
        Context $context,
        Filter $filter,
        CollectionFactory $collectionFactory,
        ChangesRepository $changesRepository
    )
    {
        $this->filter = $filter;
        $this->collectionFactory = $collectionFactory;
        $this->changesRepository = $changesRepository;

        parent::__construct($context);
    }
}