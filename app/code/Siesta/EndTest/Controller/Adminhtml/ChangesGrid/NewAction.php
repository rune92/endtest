<?php

namespace Siesta\EndTest\Controller\Adminhtml\ChangesGrid;

use Magento\Backend\App\Action\Context;
use Magento\Backend\Model\View\Result\ForwardFactory;
use Magento\Framework\Registry;
use Siesta\EndTest\Controller\Adminhtml\Changes;

/**
 * Class NewAction
 * @package Siesta\EndTest\Controller\Adminhtml\ChangesGrid
 */
class NewAction extends Changes
{
    /**
     * @var ForwardFactory
     */
    protected $resultForwardFactory;

    /**
     * @param Context        $context
     * @param Registry       $coreRegistry
     * @param ForwardFactory $resultForwardFactory
     */
    public function __construct(
        Context $context,
        Registry $coreRegistry,
        ForwardFactory $resultForwardFactory
    ) {
        $this->resultForwardFactory = $resultForwardFactory;

        parent::__construct($context, $coreRegistry);
    }

    /**
     * Create new item
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        /** @var \Magento\Framework\Controller\Result\Forward $resultForward */
        return $this->resultForwardFactory->create()->forward('edit');
    }
}
