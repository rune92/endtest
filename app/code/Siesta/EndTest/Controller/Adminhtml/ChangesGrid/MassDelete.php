<?php

namespace Siesta\EndTest\Controller\Adminhtml\ChangesGrid;

use Siesta\EndTest\Controller\Adminhtml\ChangesGrid\Mass\AbstractMass;
use Siesta\EndTest\Model\Changes;

/**
 * Class MassDelete
 * @package Oander\Interspire\Controller\Adminhtml\HistoryGrid
 */
class MassDelete extends AbstractMass
{
    /**
     * Dispatch request
     * @return \Magento\Backend\Model\View\Result\Redirect
     * @throws \Magento\Framework\Exception\LocalizedException|\Exception
     */
    public function execute()
    {
        $ids = $this->getRequest()->getParam('id');
        if (!is_array($ids) || empty($ids)) {
            $this->messageManager->addError(__('Please select row(s).'));
            $this->_redirect('*/*/');
        }

        try {
            foreach ($ids as $id) {
                $row = $this->_objectManager->get(Changes::class)->load($id);
                $this->changesRepository->delete($row);
            }
            $this->messageManager->addSuccess(
                __('A total of %1 record(s) have been deleted.', count($ids))
            );
        } catch (\Exception $e) {
            $this->messageManager->addError($e->getMessage());
        }

        $this->_redirect('*/*/');
    }
}