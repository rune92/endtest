<?php

namespace Siesta\EndTest\Controller\Adminhtml\ChangesGrid;

use Magento\Backend\App\Action\Context;
use Magento\Framework\Registry;
use Siesta\EndTest\Controller\Adminhtml\Changes;
use Siesta\EndTest\Model\ChangesRepository;
use Siesta\EndTest\Model\Data\ChangesInterface;
use Siesta\EndTest\Model\Changes as ChangesModel;
use Magento\Backend\Model\Session;

/**
 * Class Save
 * @package Oander\Interspire\Controller\Adminhtml\HistoryGrid
 */
class Save extends Changes
{
    /**
     * @var ChangesRepository
     */
    protected $changesRepository;

    /**
     * Save constructor.
     * @param Context $context
     * @param Registry $coreRegistry
     * @param ChangesRepository $changesRepository
     */
    public function __construct(
        Context $context,
        Registry $coreRegistry,
        ChangesRepository $changesRepository
    ) {
        $this->_coreRegistry = $coreRegistry;
        $this->changesRepository = $changesRepository;

        parent::__construct($context, $coreRegistry);
    }

    /**
     * Save action
     *
     * Save the model and the image.
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        $data = $this->getRequest()->getPostValue();

        if (!$data) {
            return $resultRedirect->setPath('*/*/');

        } else {
            $id = $this->getRequest()->getParam(ChangesInterface::COLUMN_ID);
            /** @var ChangesModel $model */
            $model = $this->_objectManager
                ->create(ChangesModel::class)->load($id);

            if (!$model->getId() && $id) {
                $this->messageManager->addError('Item does not exist');

                return $resultRedirect->setPath('*/*/');
            }

            $model->setData($data);

            try {
                $model = $this->changesRepository->save($model);

                $this->messageManager->addSuccess('Success to save entity');
                $this->_objectManager
                    ->get(Session::class)
                    ->setFormData(false);

                if ($this->getRequest()->getParam('back')) {
                    return $resultRedirect->setPath(
                        '*/*/edit',
                        array(ChangesInterface::COLUMN_ID => $model->getId())
                    );
                }

                return $resultRedirect->setPath('*/*/');

            } catch (\Exception $e) {
                $this->messageManager->addError($e->getMessage());
                $this->_objectManager->get(Session::class)->setFormData($data);

                return $resultRedirect->setPath('*/*/edit', array(
                    ChangesInterface::COLUMN_ID => $this->getRequest()->getParam(ChangesInterface::COLUMN_ID)
                ));
            }
        }
    }
}
