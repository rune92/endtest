<?php

namespace Siesta\EndTest\Controller\Adminhtml\ChangesGrid;

use Siesta\EndTest\Controller\Adminhtml\Changes;
use Siesta\EndTest\Model\ChangesRepository;
use Magento\Backend\App\Action\Context;
use Magento\Framework\Registry;

class Delete extends Changes
{
    /**
     * @var ChangesRepository
     */
    protected $changesRepository;

    /**
     * Delete constructor.
     *
     * @param Context         $context
     * @param Registry        $coreRegistry
     * @param ChangesRepository $changesRepository
     */
    public function __construct(
        Context $context,
        Registry $coreRegistry,
        ChangesRepository $changesRepository
    ) {
        $this->changesRepository = $changesRepository;

        parent::__construct($context, $coreRegistry);
    }

    public function execute()
    {
        $id = $this->getRequest()->getParam('id');

        try {
            $changes = $this->changesRepository->getById($id);
            $this->changesRepository->delete($changes);

            $this->messageManager->addSuccess(
                __('Delete successfully !')
            );
        } catch (\Exception $e) {
            $this->messageManager->addError($e->getMessage());
        }
        $this->_redirect('*/*/');
    }
}
