<?php

namespace Siesta\EndTest\Controller\Adminhtml\ChangesGrid;

use Magento\Backend\App\Action\Context;
use Magento\Framework\Registry;
use Magento\Framework\View\Result\PageFactory;
use Siesta\EndTest\Controller\Adminhtml\Changes;
use Siesta\EndTest\Model\ChangesRepository;
use Siesta\EndTest\Model\Data\ChangesInterface;
use Siesta\EndTest\Model\Changes as ChangesModel;
use Magento\Backend\Model\Session;

class Edit extends Changes
{
    /**
     * @var PageFactory
     */
    protected $resultPageFactory;

    /**
     * @var ChangesRepository
     */
    protected $changesRepository;

    /**
     * Edit constructor.
     * @param Context $context
     * @param Registry $coreRegistry
     * @param PageFactory $resultPageFactory
     * @param ChangesRepository $changesRepository
     */
    public function __construct(
        Context $context,
        Registry $coreRegistry,
        PageFactory $resultPageFactory,
        ChangesRepository $changesRepository
    ) {
        $this->resultPageFactory = $resultPageFactory;
        $this->changesRepository = $changesRepository;

        parent::__construct($context, $coreRegistry);
    }

    /**
     * Edit item
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        $id = $this->getRequest()->getParam(ChangesInterface::COLUMN_ID);
        $model = $this->_objectManager->create(ChangesModel::class);

        if ($id) {
            $model = $this->changesRepository->getById($id);

            if (!$model->getId()) {
                $this->messageManager->addError('Item does not exist');
                /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
                $resultRedirect = $this->resultRedirectFactory->create();

                return $resultRedirect->setPath('*/*/');
            }
        }

        $data = $this->_objectManager
            ->get(Session::class)
            ->getFormData(true);

        if (!empty($data)) {
            foreach ($data as $dataItemKey => $dataItemValue) {
                if (isset($dataItemValue['value'])) {
                    $model->setData($dataItemKey, $dataItemValue['value']);
                } else {
                    $model->setData($dataItemKey, $dataItemValue);
                }
            }
        }

        $this->_coreRegistry->register('changes_item', $model);

        /** @var \Magento\Backend\Model\View\Result\Page $resultPage */
        $resultPage = $this->resultPageFactory->create();

//        $this->initPage($resultPage)->addBreadcrumb(
//            $id ? __(TranslateImage::ITEM_EDIT) : __(TranslateImage::ITEM_NEW),
//            $id ? __(TranslateImage::ITEM_EDIT) : __(TranslateImage::ITEM_NEW)
//        );
//
//        $resultPage->getConfig()
//            ->getTitle()
//            ->prepend(__(TranslateImage::ITEMS));
//
//        $resultPage->getConfig()
//            ->getTitle()
//            ->prepend($model->getId() ? $model->getName() : __(TranslateImage::ITEM_NEW));

        return $resultPage;
    }
}
