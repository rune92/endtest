<?php
/**
 * Created by PhpStorm.
 * User: davidkovacs
 * Date: 2017. 09. 17.
 * Time: 16:28
 */

namespace Siesta\EndTest\Controller\Adminhtml;


use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Framework\Registry;
use Magento\Framework\View\Result\Page;

/**
 * Class Changes
 * @package Siesta\EndTest\Controller\Adminhtml
 */
abstract class Changes extends Action
{
    /**
     * Core registry
     *
     * @var \Magento\Framework\Registry
     */
    protected $_coreRegistry;

    /**
     * Image constructor.
     *
     * @param Context  $context
     * @param Registry $coreRegistry
     */
    public function __construct(
        Context $context,
        Registry $coreRegistry
    ) {
        $this->_coreRegistry = $coreRegistry;

        parent::__construct($context);
    }

    /**
     * @param Page $resultPage
     *
     * @return Page
     */
    protected function initPage(Page $resultPage)
    {
        return $resultPage;
    }
}