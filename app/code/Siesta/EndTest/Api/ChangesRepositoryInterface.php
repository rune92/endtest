<?php
/**
 * Created by PhpStorm.
 * User: davidkovacs
 * Date: 2017. 09. 17.
 * Time: 15:51
 */

namespace Siesta\EndTest\Api;


use Magento\Framework\Exception\NoSuchEntityException;
use Siesta\EndTest\Model\Changes;

interface ChangesRepositoryInterface
{
    /**
     * @param int $id
     * @return Changes
     * @throws NoSuchEntityException
     */
    public function getById($id);

    /**
     * @param Changes $changes
     * @return Changes
     * @throws \Exception
     */
    public function save(Changes $changes);

    /**
     * @param Changes $changes
     * @throws \Exception
     */
    public function delete(Changes $changes);

    /**
     * @param $id
     * @throws \Exception
     */
    public function deleteById($id);
}