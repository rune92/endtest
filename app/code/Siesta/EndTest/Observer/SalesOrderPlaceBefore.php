<?php


namespace Siesta\EndTest\Observer;


use Magento\Catalog\Model\ProductRepository;
use Magento\CatalogInventory\Model\Stock\StockItemRepository;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Siesta\EndTest\Helper\Data;
use Siesta\EndTest\Model\Changes;
use Siesta\EndTest\Model\ChangesRepository;
use Magento\Backend\Model\Auth\Session;

class SalesOrderPlaceBefore implements ObserverInterface
{
    /**
     * @var Changes
     */
    private $changes;

    /**
     * @var ChangesRepository
     */
    private $changesRepository;

    /**
     * @var Session
     */
    private $session;

    /**
     * @var Data
     */
    private $helper;

    /**
     * @var ProductRepository
     */
    private $productRepository;

    /**
     * @var StockItemRepository
     */
    protected $stockItemRepository;

    /**
     * SalesOrderPlaceBefore constructor.
     * @param Changes $changes
     * @param ChangesRepository $changesRepository
     * @param Session $session
     * @param Data $data
     * @param ProductRepository $productRepository
     * @param StockItemRepository $stockItemRepository
     */
    public function __construct(
        Changes $changes,
        ChangesRepository $changesRepository,
        Session $session,
        Data $data,
        ProductRepository $productRepository,
        StockItemRepository $stockItemRepository

    ) {
        $this->changes = $changes;
        $this->changesRepository = $changesRepository;
        $this->session = $session;
        $this->helper = $data;
        $this->productRepository = $productRepository;
        $this->stockItemRepository = $stockItemRepository;
    }

    /**
     * It adds the comment to the Order history.
     *
     * @param Observer $observer
     *
     * @return void
     */
    public function execute(Observer $observer)
    {
        /** @var \Magento\Sales\Model\Order $order */
        $order = $observer->getOrder();

        $items = $order->getItems();
        /** @var $item */
        foreach ($items as $item)
        {
            $productId = $item->getProductId();
            $product = $this->productRepository->getById($productId);
            $productSku = $product->getSku();
            $orderedQty = $item->getQtyOrdered();
            $productStock = $this->getStockItem($productId);
            $qty = $productStock->getQty();

            $this->helper->stockChanges($productSku, $qty, ($qty - $orderedQty));
        }
    }

    /**
     * @param $productId
     * @return \Magento\CatalogInventory\Api\Data\StockItemInterface
     */
    public function getStockItem($productId)
    {
        return $this->stockItemRepository->get($productId);
    }
}