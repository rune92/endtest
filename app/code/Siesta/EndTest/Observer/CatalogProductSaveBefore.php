<?php


namespace Siesta\EndTest\Observer;


use Magento\Catalog\Model\Product;
use Magento\Framework\Event;
use Magento\Framework\Event\ObserverInterface;
use Magento\Security\Model\Plugin\AuthSession;
use Siesta\EndTest\Helper\Data;
use Siesta\EndTest\Model\Changes;
use Siesta\EndTest\Model\ChangesRepository;
use Magento\Framework\Event\Observer;
use Magento\Backend\Model\Auth\Session;

class CatalogProductSaveBefore implements ObserverInterface
{
    /**
     * @var Changes
     */
    private $changes;

    /**
     * @var ChangesRepository
     */
    private $changesRepository;

    /**
     * @var AuthSession
     */
    private $authSession;

    /**
     * @var Data
     */
    private $helper;

    /**
     * CatalogInventoryStockItemSaveAfter constructor.
     * @param Changes $changes
     * @param ChangesRepository $changesRepository
     * @param Session $authSession
     * @param Data $data
     */
    public function __construct(
        Changes $changes,
        ChangesRepository $changesRepository,
        Session $authSession,
        Data $data

    ) {
        $this->changes = $changes;
        $this->changesRepository = $changesRepository;
        $this->authSession = $authSession;
        $this->helper = $data;
    }

    /**
     * @param Observer $observer
     */
    public function execute(Observer $observer)
    {
        /** @var Event $event */
        $event = $observer->getEvent();
        /** @var Product $product */
        $product = $event->getProduct();
        $productSku = $product->getSku();
        $dataObject = $observer->getDataByKey('data_object');

        if ($product->hasDataChanges()) {
            $origData = $dataObject->getOrigData();
            $newData = $dataObject->getData();

            $oldStock = $origData['quantity_and_stock_status'] ? $origData['quantity_and_stock_status']['qty'] : null;
            $newStock = $newData['quantity_and_stock_status'] ? $newData['quantity_and_stock_status']['qty'] : null;

            if ($oldStock != $newStock) {
                $this->helper->stockChanges($productSku, $oldStock, $newStock);
            }
        }
    }
}