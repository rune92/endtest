<?php
/**
 * Created by PhpStorm.
 * User: davidkovacs
 * Date: 2017. 09. 17.
 * Time: 15:29
 */

namespace Siesta\EndTest\Setup;

use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Siesta\EndTest\Model\Data\ChangesInterface;
use Magento\Framework\DB\Ddl\Table;

/**
 * Class InstallSchema
 * @package Siesta\EndTest\Setup
 */
class InstallSchema implements InstallSchemaInterface
{
    /**
     * @param SchemaSetupInterface $setup
     * @param ModuleContextInterface $context
     */
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $installer = $setup;
        $installer->startSetup();
        $table = $installer->getConnection()->newTable(
            $installer->getTable(ChangesInterface::TABLE)
        )
            ->addColumn(
                ChangesInterface::COLUMN_ID,
                Table::TYPE_INTEGER,
                null,
                [
                    'identity' => true,
                    'unsigned' => true,
                    'nullable' => false,
                    'primary' => true
                ],
                ChangesInterface::COLUMN_ID
            )
            ->addColumn(
                ChangesInterface::COLUMN_PRODUCT_SKU,
                Table::TYPE_TEXT,
                null,
                [
                    'nullable' => false
                ],
                ChangesInterface::COLUMN_PRODUCT_SKU
            )
            ->addColumn(
                ChangesInterface::COLUMN_NEW_STOCK_QUANTITY,
                Table::TYPE_INTEGER,
                null,
                [
                    'nullable' => false
                ],
                ChangesInterface::COLUMN_NEW_STOCK_QUANTITY
            )
            ->addColumn(
                ChangesInterface::COLUMN_OLD_STOCK_QUANTITY,
                Table::TYPE_INTEGER,
                null,
                [
                    'nullable' => false
                ],
                ChangesInterface::COLUMN_OLD_STOCK_QUANTITY
            )
            ->addColumn(
                ChangesInterface::COLUMN_RUN_BY,
                Table::TYPE_TEXT,
                null,
                [
                    'nullable' => true
                ],
                ChangesInterface::COLUMN_RUN_BY
            )
            ->addColumn(
                ChangesInterface::COLUMN_CREATE_TIME,
                Table::TYPE_DATETIME,
                null,
                [
                    'nullable' => false
                ],
                ChangesInterface::COLUMN_CREATE_TIME
            )
            ->setComment(
                'Siesta EndTest Changes table'
            );

        $installer->getConnection()->createTable($table);
        $installer->endSetup();

    }
}