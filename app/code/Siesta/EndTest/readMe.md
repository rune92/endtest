#END. Test

## TASKS

* Entity - Changes 
* Install Schema 
* Grid (Changes Model)
* Events (SalesOrderPlaceBefore, CatalogProductSaveBefore)
* Helper (model save method moved to the Helper)

## Brief report

When the users make a specific order, we stored the inventory changes to Changes Model because the module subscribed to salesOrderPlaceBefore event. So we have an extra saving method under the order. The query numbers depend on ordered items but it is not a big heavy for the site. Under the heavy, we have approximately (number of order items * 2 queries) extra queries per order. If we wanna decrease for the number of queries we need to load only needed product attributes.